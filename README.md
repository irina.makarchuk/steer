Steer web application automation tests execution
===========

## Project and Tools Overview

### Project
__Steer by Mechanic Advisor__ - web application

### Tools
Java version: JDK 17  
Build tool: Gradle  
Test tool: TestNG  
Core web automation tool: Selenide 6  
CI: Gitlab  
Reporting: Allure  


## Configure project

## Run tests
### IntelliJ IDEA
To run the single test in the IDE - select test method, right-click on the test and select 'Run' or 'Debug'. 
Use the predefined task __:test__
in the Gradle configuration, for example `:test --tests "io.ngrok.testcases.appointments.AppointmentsTests"`.
### Local test run
For the local test run on your PC using the local browser - perform the command `./gradlew test`.
### Allure results reporting
* Run the test.
* Open the report by performing the command `./gradlew allureServe` or generate it by running `./gradlew allureReport`.
* Press __<Ctrl+C>__ to exit.
### Gitlab pipelines
Follow the [link](https://gitlab.com/irina.makarchuk/steer/-/pipelines) to view the pipelines.