import org.gradle.api.tasks.testing.logging.TestExceptionFormat
import org.gradle.api.tasks.testing.logging.TestLogEvent.*
import java.util.*

plugins {
    java
    id("io.qameta.allure") version "2.11.2"
}

group = "io.ngrok"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

val lombok by extra { "1.18.28" }
val restassured by extra { "5.2.0" }
val allure by extra { "2.23.0" }
val testng by extra { "7.8.0" }

dependencies {
    implementation("com.codeborne:selenide:6.15.0")
    implementation("com.google.guava:guava:31.1-jre")
    implementation("org.testng:testng:$testng")
    implementation("org.uncommons:reportng:1.1.4")
    implementation("com.google.inject:guice:5.1.0")
    implementation("commons-validator:commons-validator:1.7")
    implementation("org.aspectj:aspectjweaver:1.9.19")
    implementation("org.apache.commons:commons-lang3:3.12.0")
    implementation("org.jtwig:jtwig-core:5.87.0.RELEASE")
    implementation("io.qameta.allure:allure-rest-assured:$allure")
    implementation("ru.yandex.qatools.allure:allure-report-builder:2.3")
    implementation("net.sourceforge.htmlunit:htmlunit:2.70.0")
    implementation("com.squareup.retrofit2:converter-gson:2.7.2")
    implementation("io.rest-assured:json-schema-validator:$restassured")
    implementation("net.javacrumbs.json-unit:json-unit:2.36.1")
    implementation("org.apache.logging.log4j:log4j-core:2.20.0")
    implementation("io.rest-assured:rest-assured:$restassured")
    implementation("ru.yandex.qatools.allure:allure-java-annotations:1.5.4")
    implementation("org.awaitility:awaitility:4.2.0")
    implementation("javax.xml.bind:jaxb-api:2.4.0-b180830.0359")
    implementation("com.github.javafaker:javafaker:1.0.2")
    implementation("org.assertj:assertj-core:3.24.2")
    implementation("org.json:json:20230227")
    testImplementation("io.qameta.allure:allure-testng:$allure")
    testImplementation("org.testng:testng:$testng")
    implementation("io.qameta.allure:allure-testng:$allure")
    implementation("io.qameta.allure:allure-selenide:$allure")
    implementation("io.qameta.allure:allure-java-commons:$allure")
    testImplementation("org.projectlombok:lombok:$lombok")
    testCompileOnly("org.projectlombok:lombok:$lombok")
    compileOnly("org.projectlombok:lombok:$lombok")
    annotationProcessor("org.projectlombok:lombok:$lombok")
    testAnnotationProcessor("org.projectlombok:lombok:$lombok")
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(17))
    }
}

tasks {
    withType<Test> {

        binaryResultsDirectory.set(
            file(
                "${binaryResultsDirectory.asFile.get().absolutePath}/${
                    UUID.randomUUID()
                }"
            )
        )
    }

    test {
        useTestNG()

        testLogging {
            events(
                FAILED,
                PASSED,
                SKIPPED,
                STARTED
            )
            showStandardStreams = true
            exceptionFormat = TestExceptionFormat.FULL
            showCauses = true
            showExceptions = true
            showStackTraces = true
        }
    }
}