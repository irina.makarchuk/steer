package io.ngrok.baseutils;

import com.codeborne.selenide.ClickOptions;

import java.time.Duration;

public class ActionUtils {

    public ActionUtils() {
    }

    public static ClickOptions jsClickOptions() {
        return ClickOptions.usingJavaScript().timeout(Duration.ofSeconds(2));
    }
}
