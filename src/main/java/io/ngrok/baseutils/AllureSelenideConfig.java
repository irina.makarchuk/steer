package io.ngrok.baseutils;

import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import io.qameta.allure.selenide.LogType;

import java.util.logging.Level;


public class AllureSelenideConfig {

    private AllureSelenideConfig() {}

    public static void addListeners() {
        AllureSelenide allureSelenide = new AllureSelenide();
        SelenideLogger.addListener("AllureSelenide", allureSelenide
                .screenshots(true)
                .savePageSource(false)
                .enableLogs(LogType.BROWSER, Level.SEVERE));
    }
}
