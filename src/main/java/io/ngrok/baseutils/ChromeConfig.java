package io.ngrok.baseutils;

import com.codeborne.selenide.Configuration;

public class ChromeConfig {

    public ChromeConfig() {
    }

    public static void configure(String baseUrl) {
        Configuration.baseUrl = baseUrl;
        Configuration.timeout = 12_000;
        Configuration.pageLoadTimeout = 40_000;
    }
}
