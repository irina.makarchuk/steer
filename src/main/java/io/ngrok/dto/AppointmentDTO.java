package io.ngrok.dto;

import com.github.javafaker.Faker;
import io.ngrok.enums.appointments.AppointmentType;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class AppointmentDTO {

    @Builder.Default
    private String appointmentTitle = new Faker().chuckNorris().fact();

    @Builder.Default
    private String appointmentType = AppointmentType.WAITING.getType();

    private String customer;

    private String vehicle;

    private String status;

    private String dateAndTimeRange;

    @Builder.Default
    private String details = new Faker().book().title();
}
