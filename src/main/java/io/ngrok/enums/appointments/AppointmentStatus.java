package io.ngrok.enums.appointments;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AppointmentStatus {

    REQUEST_MADE("Request Made"),
    BOOKED("Booked"),
    FINISHED("Finished"),
    MISSED("Missed");

    private final String status;
}
