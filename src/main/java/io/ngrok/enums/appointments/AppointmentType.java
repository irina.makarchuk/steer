package io.ngrok.enums.appointments;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AppointmentType {

    WAITING("Waiting"),
    DROPPING_OFF("Dropping Off"),
    NONE("None");

    private final String type;
}
