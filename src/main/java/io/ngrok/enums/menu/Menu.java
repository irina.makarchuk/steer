package io.ngrok.enums.menu;

import lombok.Getter;
import lombok.AllArgsConstructor;

@Getter
@AllArgsConstructor
public enum Menu {

    DASHBOARD("Dashboard"),
    APPOINTMENTS("Appointments");

    private final String item;
}
