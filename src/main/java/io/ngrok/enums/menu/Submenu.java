package io.ngrok.enums.menu;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Submenu {

    SCHEDULER("Scheduler"),
    CALENDAR("Calendar"),
    NOTIFICATIONS("Notifications");

    private final String item;
}
