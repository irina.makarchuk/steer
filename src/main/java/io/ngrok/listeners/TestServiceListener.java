package io.ngrok.listeners;

import com.google.inject.Guice;
import org.testng.IConfigurationListener;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

public class TestServiceListener extends TestListenerAdapter implements IConfigurationListener {

    @Override
    public void beforeConfiguration(ITestResult testResult) {
        super.beforeConfiguration(testResult);
        Guice.createInjector().injectMembers(testResult.getMethod().getInstance());
    }
}
