package io.ngrok.pages;

import com.codeborne.selenide.SelenideElement;
import io.ngrok.baseutils.ActionUtils;
import io.ngrok.enums.menu.Menu;
import io.ngrok.enums.menu.Submenu;

import static com.codeborne.selenide.Condition.enabled;
import static com.codeborne.selenide.Selenide.$x;
import static java.util.Objects.requireNonNull;

public class LeftMenu {

    private static final String MENU_BLOCK = "//*[contains(@class, 'Drawer___33IT8')]";
    private static final String MENU_XPATH = "//*[contains(@class, 'pointer') and .//text()='%s']";
    private static final String SUBMENU_XPATH = "//nav[contains(@class, 'Navigation')]//a[.//text()='%s']";
    private static final String MENU_DRAWER_TOGGLE_XPATH = ".//div[contains(@class, 'drawerToggle')]";

    public SelenideElement getMenuBlock() {
        return $x(MENU_BLOCK);
    }

    public void openMenu(Menu menu) {
         if (requireNonNull(getMenuBlock().shouldBe(enabled).getAttribute("class")).contains("isNarrowed")) {
             getMenuBlock().$x(MENU_DRAWER_TOGGLE_XPATH).hover().click(ActionUtils.jsClickOptions());
         }

        $x(String.format(MENU_XPATH, menu.getItem())).click();
    }

    public void openSubmenu(Submenu submenu) {
        $x(String.format(SUBMENU_XPATH, submenu.getItem())).click();
    }
}
