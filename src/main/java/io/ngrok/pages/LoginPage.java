package io.ngrok.pages;

import io.ngrok.pages.webelements.login.LoginBlock;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class LoginPage {

    public LoginBlock loginBlock() {
        return new LoginBlock($(By.className("Box___2efmb")));
    }
}
