package io.ngrok.pages.appointments;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.ngrok.pages.webelements.appointments.NewAppointmentDialog;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;

import java.time.Duration;
import java.util.stream.Stream;

import static com.codeborne.selenide.CollectionCondition.sizeGreaterThan;
import static com.codeborne.selenide.Condition.enabled;
import static com.codeborne.selenide.Selenide.*;

public class SchedulerPage {

    private static final String NEW_APPOINTMENT_BUTTON_XPATH = "//button[.//text()='New Appointment']";
    private static final String REQUEST_MADE_APPOINTMENT_ROW_XPATH = "//*[contains(@class, 'RequestMade')]" +
            "//*[contains(@class, 'row') and @id]";
    private static final String NEW_APPOINTMENT_DIALOG_CLASS = "modalWrapper___2N7tk";
    private static final String REQUEST_MADE_APPOINTMENTS_HEADER_XPATH = "//*[contains(@class, 'header___2kiBm') and " +
            ".//text()='Request Made']//*[contains(@class, 'md___2YJhE')]";

    public void clickNewAppointmentButton() {
        $x(NEW_APPOINTMENT_BUTTON_XPATH).click();
    }

    public ElementsCollection getRequestAppointments() {
        return $$x(REQUEST_MADE_APPOINTMENT_ROW_XPATH);
    }

    public Stream<SelenideElement> getRequestMadeAppointmentRows() {
        getRequestAppointments().shouldBe(sizeGreaterThan(0), Duration.ofSeconds(15));
        return getRequestAppointments().asDynamicIterable().stream()
                .map(element -> element.should(enabled, Duration.ofSeconds(15)));
    }

    public NewAppointmentDialog getAppointmentDialog() {
        return new NewAppointmentDialog($(By.className(NEW_APPOINTMENT_DIALOG_CLASS)));
    }

    public int getRequestMadeAppointmentsCount() {
        return Integer.parseInt($x(REQUEST_MADE_APPOINTMENTS_HEADER_XPATH).innerHtml()
                .replaceAll(".+ ", StringUtils.EMPTY));
    }
}
