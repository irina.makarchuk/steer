package io.ngrok.pages.webelements.appointments;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.ngrok.pages.appointments.SchedulerPage;
import org.openqa.selenium.By;

import static com.codeborne.selenide.CollectionCondition.sizeGreaterThan;
import static com.codeborne.selenide.Condition.partialText;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.page;

public class NewAppointmentDialog {

    private final SelenideElement container;

    public NewAppointmentDialog(SelenideElement container) {
        this.container = container;
    }

    private static final String TITLE_XPATH = ".//*[@name='title']";
    private static final String TYPE_XPATH = ".//*[contains(@class, 'switcher')]/div[text()='%s']";
    private static final String CUSTOMER_XPATH = ".//*[@name='customer']";
    private static final String CUSTOMER_INPUT_FIELD_XPATH = ".//input[@id]";
    private static final String CUSTOMER_DROPDOWN_OPTIONS_XPATH = ".//*[contains(@id, 'option-')]";
    private static final String SELECTED_CUSTOMER_OPTION_XPATH = ".//*[contains(@class, '-singleValue')]";
    private static final String STATUS_XPATH = ".//*[@name='status']";
    private static final String STATUS_INPUT_FIELD_XPATH = ".//*[contains(@class, 'ValueContainer')]";
    private static final String STATUS_DROPDOWN_OPTIONS_XPATH = ".//*[contains(@id, 'option-')]";
    private static final String SELECTED_STATUS_OPTION_XPATH = ".//*[contains(@class, '-singleValue')]";
    private static final String DATE_AND_TIME_ICON_XPATH = ".//*[contains(@class, 'iconContainer___2nxwh')]";
    private static final String DATE_AND_TIME_RANGE_FROM_CLASS = "react-datepicker__day--today";
    private static final String DATE_AND_TIME_RANGE_TO_XPATH = ".//*[contains(@class, 'react-datepicker__week')]/*[not(contains(@class, 'outside'))]";
    private static final String DETAILS_XPATH = ".//*[@name='details']";
    private static final String CREATE_BUTTON_XPATH = ".//*[contains(@class, 'button_primary')]";

    public NewAppointmentDialog setTitle(String title) {
        container.$x(TITLE_XPATH).val(title);
        return this;
    }

    public NewAppointmentDialog setType(String appointmentType) {
        container.$x(String.format(TYPE_XPATH, appointmentType)).click();
        return this;
    }

    public void openCustomerDropdown() {
        container.$x(CUSTOMER_XPATH).$x(CUSTOMER_INPUT_FIELD_XPATH).click();
    }

    private ElementsCollection getCustomerDropdownOptions() {
        return container.$$x(CUSTOMER_DROPDOWN_OPTIONS_XPATH).shouldHave(sizeGreaterThan(0));
    }

    public NewAppointmentDialog setCustomer(String customerName) {
        openCustomerDropdown();
        getCustomerDropdownOptions().filter(partialText(customerName)).first().click();
        return this;
    }

    public NewAppointmentDialog setCustomer() {
        openCustomerDropdown();
        getCustomerDropdownOptions()
                .asDynamicIterable().stream()
                .findAny()
                .ifPresent(SelenideElement::click);
        return this;
    }

    public String getSelectedCustomerName() {
        return container.$x(CUSTOMER_XPATH).$x(SELECTED_CUSTOMER_OPTION_XPATH).text();
    }

    public NewAppointmentDialog setStatus(String appointmentStatus) {
        openStatusDropdown();
        getStatusDropdownOptions().filter(partialText(appointmentStatus)).first().click();
        return this;
    }

    public void openStatusDropdown() {
        container.$x(STATUS_XPATH).$x(STATUS_INPUT_FIELD_XPATH).click();
    }

    private ElementsCollection getStatusDropdownOptions() {
        return container.$$x(STATUS_DROPDOWN_OPTIONS_XPATH)
                .shouldHave(sizeGreaterThan(0));
    }

    public String getSelectedStatusName() {
        return container.$x(STATUS_XPATH).$x(SELECTED_STATUS_OPTION_XPATH).text();
    }

    public NewAppointmentDialog setDateAndTimeRange() {
        clickCalendarIcon();
        container.$(By.className(DATE_AND_TIME_RANGE_FROM_CLASS)).click();
        container.$$x(DATE_AND_TIME_RANGE_TO_XPATH).last().click();
        clickCalendarIcon();
        return this;
    }

    private static void clickCalendarIcon() {
        $x(DATE_AND_TIME_ICON_XPATH).click();
    }

    public NewAppointmentDialog setDetails(String details) {
        container.$x(DETAILS_XPATH).val(details);
        return this;
    }

    public SchedulerPage create() {
        container.$x(CREATE_BUTTON_XPATH).click();
        return page(SchedulerPage.class);
    }
}
