package io.ngrok.pages.webelements.login;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

public class LoginBlock {

    private final SelenideElement container;

    public LoginBlock(SelenideElement container) {
        this.container = container;
    }

    private static final String EMAIL_ADDRESS_ID = "username";
    private static final String PASSWORD_ID = "password";
    private static final String LOGIN_BUTTON_XPATH = ".//*[contains(@class, 'button_primary')]";

    public LoginBlock setEmailAddress(String email) {
        container.$(By.id(EMAIL_ADDRESS_ID)).val(email);
        return this;
    }

    public LoginBlock setPassword(String password) {
        container.$(By.id(PASSWORD_ID)).val(password);
        return this;
    }

    public void clickLogin() {
        container.$x(LOGIN_BUTTON_XPATH).click();
    }
}
