package io.ngrok.steps.appointments;

import com.google.inject.Inject;
import io.ngrok.dto.AppointmentDTO;
import io.ngrok.pages.appointments.SchedulerPage;
import io.qameta.allure.Step;

import java.util.Optional;

public class CreateNewAppointmentSteps {

    @Inject
    private SchedulerPage schedulerPage;
    @Inject
    private SchedulerSteps schedulerSteps;

    @Step
    public CreateNewAppointmentSteps setTitle(String title) {
        schedulerPage.getAppointmentDialog().setTitle(title);
        return this;
    }

    @Step
    public CreateNewAppointmentSteps setAppointmentType(String appointmentType) {
        schedulerPage.getAppointmentDialog().setType(appointmentType);
        return this;
    }

    @Step
    public CreateNewAppointmentSteps setStatus(String appointmentStatus) {
        schedulerPage.getAppointmentDialog().setStatus(appointmentStatus);
        return this;
    }

    @Step
    public CreateNewAppointmentSteps setCustomer(String customer) {
        schedulerPage.getAppointmentDialog().setCustomer(customer);
        return this;
    }

    @Step
    public String setCustomer() {
        schedulerPage.getAppointmentDialog().setCustomer();
        return schedulerPage.getAppointmentDialog().getSelectedCustomerName();
    }

    @Step
    public CreateNewAppointmentSteps setDetails(String details) {
        schedulerPage.getAppointmentDialog().setDetails(details);
        return this;
    }

    @Step
    public CreateNewAppointmentSteps setDateAndTimeRange() {
        schedulerPage.getAppointmentDialog().setDateAndTimeRange();
        return this;
    }

    @Step
    public String getSelectedStatusName() {
        return schedulerPage.getAppointmentDialog().getSelectedStatusName();
    }

    @Step
    public void create() {
        schedulerPage.getAppointmentDialog().create();
    }

    @Step
    public void createAppointment(AppointmentDTO appointmentDTO) {
        schedulerSteps.clickNewAppointmentButton();
        setTitle(appointmentDTO.getAppointmentTitle());
        Optional.ofNullable(appointmentDTO.getAppointmentType()).ifPresent(this::setAppointmentType);
        Optional.ofNullable(appointmentDTO.getCustomer())
                .ifPresentOrElse(this::setCustomer, () -> {
                    String customer = setCustomer();
                    appointmentDTO.setCustomer(customer);
                });
        Optional.ofNullable(appointmentDTO.getStatus())
                .ifPresentOrElse(this::setStatus, () -> {
                    String status = getSelectedStatusName();
                    appointmentDTO.setStatus(status);
                });
        setDateAndTimeRange();
        Optional.ofNullable(appointmentDTO.getDetails()).ifPresent(this::setDetails);
        create();
    }
}
