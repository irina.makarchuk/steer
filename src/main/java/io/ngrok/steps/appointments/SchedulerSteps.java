package io.ngrok.steps.appointments;


import com.google.inject.Inject;
import io.ngrok.pages.appointments.SchedulerPage;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.Wait;

public class SchedulerSteps {

    @Inject
    private SchedulerPage schedulerPage;

    @Step
    public void clickNewAppointmentButton() {
        schedulerPage.clickNewAppointmentButton();
    }

    @Step
    public int getAppointmentsCount() {
        waitForUpdatedRequestMadeAppointmentRowsList();
        return schedulerPage.getRequestMadeAppointmentsCount();
    }

    @Step
    public void updateAppointmentsCount(int previousAppointmentsCount) {
        Wait().until(option -> getAppointmentsCount() != previousAppointmentsCount);
    }

    @Step
    public void waitForUpdatedRequestMadeAppointmentRowsList() {
        schedulerPage.getRequestMadeAppointmentRows();
    }
}
