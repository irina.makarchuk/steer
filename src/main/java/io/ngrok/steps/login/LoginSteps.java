package io.ngrok.steps.login;

import com.google.inject.Inject;
import io.ngrok.pages.LoginPage;
import io.qameta.allure.Step;

public class LoginSteps {

    @Inject
    private LoginPage loginPage;

    @Step
    public void login(String emailAddress, String password) {
        loginPage.loginBlock()
                .setEmailAddress(emailAddress)
                .setPassword(password)
                .clickLogin();
    }
}
