package io.ngrok.steps.menu;

import com.google.inject.Inject;
import io.ngrok.enums.menu.Menu;
import io.ngrok.enums.menu.Submenu;
import io.ngrok.pages.LeftMenu;
import io.qameta.allure.Step;

public class LeftMenuSteps {

    @Inject
    private LeftMenu leftMenu;

    @Step
    public void open(Menu menu) {
        leftMenu.openMenu(menu);
    }

    @Step
    public void open(Menu menu, Submenu submenu) {
        leftMenu.openMenu(menu);
        leftMenu.openSubmenu(submenu);
    }
}
