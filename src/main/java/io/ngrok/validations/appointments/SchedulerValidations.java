package io.ngrok.validations.appointments;

import com.google.inject.Inject;
import io.ngrok.steps.appointments.SchedulerSteps;
import io.qameta.allure.Step;
import org.assertj.core.api.Assertions;

public class SchedulerValidations {

    @Inject
    private SchedulerSteps schedulerSteps;

    @Step
    public void verifyAppointmentIsAdded(int previousAppointmentsCount) {
        schedulerSteps.updateAppointmentsCount(previousAppointmentsCount);
        Assertions.assertThat(schedulerSteps.getAppointmentsCount())
                .as("The appointment hasn't been added")
                .isEqualTo(previousAppointmentsCount + 1);
    }
}
