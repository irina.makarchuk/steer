package io.ngrok.testcases;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import com.google.inject.Guice;
import com.google.inject.Inject;
import io.ngrok.baseutils.AllureSelenideConfig;
import io.ngrok.baseutils.ChromeConfig;
import io.ngrok.listeners.TestServiceListener;
import io.ngrok.steps.login.LoginSteps;
import io.ngrok.steps.menu.LeftMenuSteps;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;

import static com.codeborne.selenide.Selenide.open;

@Listeners({TestServiceListener.class})
public abstract class BaseTest {

    @Inject
    protected LoginSteps loginSteps;
    @Inject
    protected LeftMenuSteps leftMenuSteps;

    private static final String BASE_URL = "https://dev2frontend.ngrok.io";

    static {
        ChromeConfig.configure(BASE_URL);
        AllureSelenideConfig.addListeners();
    }

    @BeforeSuite
    public void setUpBeforeSuite() {
        loginSteps = Guice.createInjector(new BasicModule()).getInstance(LoginSteps.class);
        leftMenuSteps = Guice.createInjector(new BasicModule()).getInstance(LeftMenuSteps.class);
    }

    @BeforeMethod
    public void setUpBeforeMethod() {
        open("/login");
        // todo move to .env or System.properties
        loginSteps.login("aqa.interview@mailinator.com", "P5vioN%jc&^b");
    }

    @AfterMethod
    public void setUpAfterMethod() {
        WebDriverRunner.clearBrowserCache();
        Selenide.clearBrowserCookies();
        Selenide.clearBrowserLocalStorage();
    }
}
