package io.ngrok.testcases.appointments;

import com.google.inject.Inject;
import io.ngrok.dto.AppointmentDTO;
import io.ngrok.enums.appointments.AppointmentStatus;
import io.ngrok.enums.menu.Menu;
import io.ngrok.enums.menu.Submenu;
import io.ngrok.steps.appointments.CreateNewAppointmentSteps;
import io.ngrok.steps.appointments.SchedulerSteps;
import io.ngrok.testcases.BaseTest;
import io.ngrok.validations.appointments.SchedulerValidations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class AppointmentsTests extends BaseTest {

    @Inject
    private SchedulerSteps schedulerSteps;
    @Inject
    private CreateNewAppointmentSteps createNewAppointmentSteps;
    @Inject
    private SchedulerValidations schedulerValidations;

    @BeforeMethod
    public void goToPage() {
        leftMenuSteps.open(Menu.APPOINTMENTS, Submenu.SCHEDULER);
    }

    @Test
    public void verifyAppointmentIsCreated() {
        var previousAppointmentsCount = schedulerSteps.getAppointmentsCount();
        AppointmentDTO appointmentDTO = AppointmentDTO.builder()
                .status(AppointmentStatus.REQUEST_MADE.getStatus())
                .build();

        createNewAppointmentSteps.createAppointment(appointmentDTO);
        schedulerValidations.verifyAppointmentIsAdded(previousAppointmentsCount);
    }
}
